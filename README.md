# MicroService Identity

Identity manager for authentication flows.

## Installation

Installing this package globally also installs a SystemD template unit file. To start the SystemD service then, run the following commands as sudo:

    npm install -g gitlab:node-microservice/identity
    systemctl daemon-reload
    systemctl enable microservice-identity@1234
    systemctl start microservice-identity@1234

Substitute 1234 with the port under which you want to run the microservice orchestrator. You will need to set additional parameters if you want email verification for new user registration.

## Usage

The following parameters can be provided to the startup script, not including common to other microservices like `db` and `port`:

- `mgkey`: Mailgun API key.
- `mgdomain`: Mailgun verified domain.
- `email`: Enable email verification for new registrations.
- `verify-url`: Base URL for verification link. Defaults to `https://${NAKED_DOMAIN}/identity/verify`. `NAKED_DOMAIN` is derived from `mgdomain`.
- `redirect_url`: Base URL to redirect users after successful email verification. Defaults to `https://${NAKED_DOMAIN}`. `NAKED_DOMAIN` is derived from `mgdomain`.

Once started, this microservice registers the following endpoints:

- `/register?id=<id>&hash=<hash>[&email=<email>]`: Registers a new user with an ID a password hash. Email is optional; you will need to setup mailgun and provide key and domain to the startup script.
- `/verify?email=<email>&token=<token>`: Verifies an email address to finalize registration.
- `/login?id=<id>&hash=<hash>`: Provides a token with 30-day expiration to the user requesting it.
- `/logout?token=<token>`: Revokes the provided token and logs out user.
- `/auth?token=<token>`: Verifies that the provided token belongs to a valid user and returns their ID.

## TODO

- DDOS mitigations
- Password policies
- Forgot password