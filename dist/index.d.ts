import { MicroService } from 'microservice';
export interface User {
    id: string;
    hash: string;
    email?: string;
    token?: string;
    verified: boolean;
}
/**
 * Main entrypoint to start this service
 * @param opts Catch-all for parameters. Options include:
 *
 * `port` - TCP port where this service should run
 *
 * `db` - database location, defaults to in-memory database
 *
 * `orchestrator` - URL where microservice-orchestrator is running; must be full URL including
 * protocol. E.g. "http://127.0.0.1:3000"
 */
export declare function main(opts?: {
    [key: string]: any;
}): Promise<MicroService>;
