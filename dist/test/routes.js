"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const request = require("supertest");
const index_1 = require("../index");
const utils_1 = require("../utils");
let service;
describe('Without email verification', () => {
    const userid1 = '1234';
    const userid2 = '5678';
    const hash = 'abcd';
    const port = Math.floor(Math.random() * 8975) + 1024;
    before(() => __awaiter(this, void 0, void 0, function* () {
        service = yield index_1.main({ test: true, email: false });
    }));
    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(port);
    });
    afterEach(() => {
        service.stop();
    });
    describe('Register', () => {
        it('OK', () => __awaiter(this, void 0, void 0, function* () {
            const res = yield request(service.server).get(`/register?id=${userid1}&hash=${hash}`).expect(200);
            const user = yield service.db.get(`SELECT * FROM users WHERE id = ?`, [userid1]);
            assert.equal(!!user.verified, true);
            assert(res.header['set-cookie'].join('').match(/token=[\w\-]+;/));
        }));
        it('Fails for existing ID', () => __awaiter(this, void 0, void 0, function* () {
            yield request(service.server).get(`/register?id=${userid1}&hash=${hash}`).expect(400);
            const user = yield service.db.get(`SELECT * FROM users WHERE id = ?`, [userid1]);
            assert(!!user);
        }));
    });
    describe('Login', () => {
        it('OK', () => __awaiter(this, void 0, void 0, function* () {
            const res = yield request(service.server).get(`/login?id=${userid1}&hash=${hash}`).expect(200);
            assert(res.header['set-cookie'].join('').match(/token=[\w\-]+;/));
        }));
        it('Wrong password', done => {
            request(service.server).get(`/login?id=${userid1}&hash=x${hash}`).expect(400, done);
        });
        it('Wrong user', done => {
            request(service.server).get(`/login?id=${userid2}&hash=${hash}`).expect(400, done);
        });
    });
    describe('Auth', () => {
        it('OK', () => __awaiter(this, void 0, void 0, function* () {
            const token = (yield service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            const res = yield request(service.server).get(`/auth?token=${token}`).expect(200);
        }));
        it('Using helper function', () => __awaiter(this, void 0, void 0, function* () {
            const token = (yield service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            assert(utils_1.auth(`http://127.0.0.1:${port}`, token, userid1));
        }));
    });
    describe('Logout', () => {
        it('OK', () => __awaiter(this, void 0, void 0, function* () {
            const token = (yield service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            const res = yield request(service.server).get(`/logout?token=${token}`).expect(200);
            assert(res.header['set-cookie'].join('').match(/token=;/));
            yield request(service.server).get(`/auth?token=${token}`).expect(400);
        }));
    });
});
describe('With email verification', () => {
    const email = 'fake@email.com';
    const userid1 = '1234';
    const userid2 = '5678';
    const hash = 'abcd';
    before(() => __awaiter(this, void 0, void 0, function* () {
        service = yield index_1.main({ test: true, email: true });
    }));
    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });
    describe('Register', () => {
        it('Fails without email', () => __awaiter(this, void 0, void 0, function* () {
            yield request(service.server).get(`/register?id=${userid1}&hash=${hash}`).expect(400);
        }));
    });
    describe('Verify', () => {
        it('Provided token', () => __awaiter(this, void 0, void 0, function* () {
            // Register user
            const res = yield request(service.server).get(`/register?id=${userid1}&hash=${hash}&email=${email}`).expect(200);
            const user = yield service.db.get(`SELECT * FROM users WHERE id = ?`, [userid1]);
            assert.equal(!!user.verified, false);
            assert(!res.header['set-cookie'] || !res.header['set-cookie'].join('').match(/token/));
            // Verify token
            const token = (yield service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            yield request(service.server).get(`/verify?token=${token}&email=${email}`).expect(200);
        }));
    });
});
