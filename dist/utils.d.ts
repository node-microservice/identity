/// <reference types="node" />
import * as url from 'url';
/**
 * Returns true/false if the provided token belongs to the specified user ID
 * @param endpoint URL of the authentication endpoint
 * @param token session token to authenticate
 * @param user_id expected user ID
 */
export declare function auth(endpoint: string | url.Url, token: string, user_id: string, orchestrator?: boolean): Promise<boolean>;
