"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ootils_1 = require("ootils");
const url = require("url");
/**
 * Returns true/false if the provided token belongs to the specified user ID
 * @param endpoint URL of the authentication endpoint
 * @param token session token to authenticate
 * @param user_id expected user ID
 */
function auth(endpoint, token, user_id, orchestrator = false) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let auth_url = null;
            if (typeof endpoint === 'string') {
                auth_url = url.parse(endpoint);
            }
            else {
                auth_url = endpoint;
            }
            if (orchestrator) {
                auth_url.pathname = `/service/identity/auth`;
            }
            auth_url.query = { token: token };
            const res = JSON.parse(yield ootils_1.Requests.get(url.format(auth_url)));
            return res.status === 'OK' && res.id === user_id;
        }
        catch (ex) {
            return false;
        }
    });
}
exports.auth = auth;
