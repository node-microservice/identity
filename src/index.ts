import { MicroService, Response } from 'microservice';
import { Mail } from 'ootils';
import * as minimist from 'minimist';
import * as os from 'os';
import * as uuid from 'uuid/v4';
import * as url from 'url';

export interface User {
    id: string,
    hash: string,
    email?: string,
    token?: string,
    verified: boolean
}

/**
 * Main entrypoint to start this service
 * @param opts Catch-all for parameters. Options include:
 * 
 * `port` - TCP port where this service should run
 * 
 * `db` - database location, defaults to in-memory database
 * 
 * `orchestrator` - URL where microservice-orchestrator is running; must be full URL including
 * protocol. E.g. "http://127.0.0.1:3000"
 */
export async function main(opts: {[key: string]: any} = {}): Promise<MicroService> {

    // Initialize status
    const GLOBAL_STATS: {[key: string]: any} = {};
    GLOBAL_STATS.hostname = os.hostname();
    ['login', 'logout', 'registration', 'verification'].forEach(metric => {
        GLOBAL_STATS[`total_${metric}_attempts`] = 0;
        GLOBAL_STATS[`total_${metric}_success`] = 0;
    });

    /**
     * Helper function used to read arguments. The order of preference is: 1) opts 2) args 3) env
     * @param name name of the parameter being retrieved
     * @param env if true, search for parameter setting in ENV as well
     */
    function getopt(name: string, env = true): string {
        return opts[name] || args[name] || (env && process.env[name]);
    }

    // Parse command-line arguments and define global parameters
    const args = minimist(process.argv.slice(1));
    const TOKEN_EXPIRY_SECONDS = 30 * 24 * 60 * 60; // 30 days
    const TEST_MODE = !!getopt('test');
    const MG_KEY = getopt('mgkey');
    const MG_DOMAIN = getopt('mgdomain');
    const REQUIRE_EMAIL = !!getopt('email');
    const NAKED_DOMAIN = MG_DOMAIN && MG_DOMAIN.substr(MG_DOMAIN.indexOf('.') + 1);
    const VERIFY_URL = getopt('verify-url') || `https://${NAKED_DOMAIN}/identity/verify`;
    const REDIRECT_URL = getopt('redirect-url') || `https://${NAKED_DOMAIN}`;
    
    
    // Initialize microservice (with no caching allowed)
    const service = await MicroService.create(getopt('db'), -1);

    // Setup debugging
    process.on('unhandledRejection', err => service.log('E', err));
    
    // Make sure that we have all required arguments for provided parameters
    if (!TEST_MODE && REQUIRE_EMAIL && (!MG_DOMAIN || !MG_KEY)) {
        service.log('E', `Required arguments "mgkey" and "mgdomain" not provided`)
        process.exit(1);
    }

    // Initialize database tables
    await service.db.run(`
        CREATE TABLE IF NOT EXISTS users (
            id           TEXT NOT NULL PRIMARY KEY,
            hash         TEXT NOT NULL,
            email        TEXT,
            token        TEXT,
            verified     INT,
            expiry       DATETIME,
            timestamp    DATETIME DEFAULT CURRENT_TIMESTAMP)`);

    /**
     * Provide session token to `user` within `response`.
     * @param token session token that identifies user
     * @param user object with user fields
     * @param response MicroService Response object used to manipulate cookies
     */
    async function giveToken(token: string, user: User, response: Response) {
        user.token = token;

        // Store token in cookies and the db
        response.cookie('token', token, {maxAge: 900000, httpOnly: true});
        await service.db.run(`
            UPDATE users SET
            token = ?, expiry = datetime('now', '+${TOKEN_EXPIRY_SECONDS} seconds')
            WHERE id = ?`, [token, user.id]);
    }

    /**
     * Verify if the provided token is valid and, if it is, return the user it belongs to.
     * @param token session token that identifies user
     */
    async function authToken(token: string): Promise<User> {
        const tmp = await service.db.all('select * from users');
        const user: User = await service.db.get(`
            SELECT id, hash, verified FROM users
            WHERE token = ? AND verified = 1 AND expiry > CURRENT_TIMESTAMP`, [token]);
            if (!user) {
            const msg = `Auth failed`;
            throw Error(msg);
        } else {
            return user;
        }
    }

    // Routes setup
    // TODO: DOS protection

    // Status
    // Important: do not delete this route if you plan on using orchestrator!
    service.route('/status', (request, response) => {
        response.send({status: 'OK', data: GLOBAL_STATS});
    });

    // Registers a user given ID and password hash
    await service.route('/register', async (request, response) => {
        GLOBAL_STATS.total_registration_attempts++;
        const user_id: User = await service.db.get(`
            SELECT id, hash, verified FROM users WHERE id = ?`, [request.query.id]);
        const user_email: User = REQUIRE_EMAIL && await service.db.get(`
            SELECT id, hash, verified FROM users WHERE email = ?`, [request.query.email]);
        if (user_id || user_email) {
            const msg = `User already exists`;
            service.log('E', msg, user_id);
            response.status(400).send(
                {status: 'ERR', data: msg});

        } else {

            // Email parameter is only mandatory in certain conditions
            if (REQUIRE_EMAIL && !request.query.email) {
                const msg = `Missing mandatory parameter "email"`;
                service.log('E', msg);
                response.status(400).send({status: 'ERR', msg});

            } else {

                // Generate token for verification and insert into database
                const token = uuid();
                const user: User = {
                    id: request.query.id,
                    hash: request.query.hash,
                    email: request.query.email,
                    verified: !REQUIRE_EMAIL
                }
                await service.db.run(`
                    INSERT INTO users (id, hash, token, email, verified) VALUES (?, ?, ?, ?, ?)`,
                    [user.id, user.hash, token, user.email, user.verified]);

                try {
                    // Send email with verification link if appropriate
                    if (REQUIRE_EMAIL && !TEST_MODE) {
                        const subject = `Verify your email address`;
                        const link = `${VERIFY_URL}?email=${user.email}&token=${token}&redirect_url=${REDIRECT_URL}`;
                        const content = `${subject}\n\n<a href="${link}">${link}</a>`;
                        await Mail.send(MG_KEY, MG_DOMAIN, `noreply@${MG_DOMAIN}`, user.email, subject, content);

                    } else if (!REQUIRE_EMAIL) {
                        await giveToken(token, user, response);
                    }

                    const msg = `Successfully registered user ${user.id}`;
                    service.log('I', msg);
                    response.send({status: 'OK', data: msg});
                    GLOBAL_STATS.total_registration_success++;

                } catch (ex) {
                    service.log('E', ex);
                    response.status(500).send({status: 'ERR', data: ex.message});
                }
            }
        }
    }, {method: 'GET', mandatoryQueryParameters: ['id', 'hash'], optionalQueryParameters: ['email']});

    // Verifies user registration using token
    await service.route('/verify', async (request, response) => {
        GLOBAL_STATS.total_verification_attempts++;
        const user: User = await service.db.get(`
            SELECT id, hash, verified FROM users WHERE email = ? AND token = ? AND verified = 0`,
            [request.query.email, request.query.token]);
        if (!user) {
            const msg = `Could not verify email ${request.query.email}`;
            service.log('E', msg, request.query.token);
            response.status(400).send(
                {status: 'ERR', data: msg});
        } else {
            // Mark user as verified
            await service.db.run(`
                UPDATE users SET verified = 1 WHERE email = ? AND token = ?`,
                [request.query.email, request.query.token]);

            // Provide a new token and login user automatically
            const token = uuid();
            await giveToken(token, user, response);

            const msg = `Verification for ${request.query.email} successfull`;
            service.log('I', msg);
            if (request.query.redirect_url) {
                response.redirect(request.query.redirect_url);
            } else {
                response.send({status: 'OK', data: msg});
            }
            GLOBAL_STATS.total_verification_success++;
        }
    }, {method: 'GET', mandatoryQueryParameters: ['email', 'token'], optionalQueryParameters: ['redirect_url']});

    // Logins a user given ID and password hash and returns token
    await service.route('/login', async (request, response) => {
        GLOBAL_STATS.total_login_attempts++;
        const user: User = await service.db.get(`
            SELECT id, hash, verified FROM users WHERE id = ? AND hash = ? AND verified = 1`,
            [request.query.id, request.query.hash]);
        if (!user) {
            const msg = `User ${request.query.id} does not exist or wrong password`;
            service.log('E', msg);
            response.status(400).send(
                {status: 'ERR', data: msg});
        } else {
            // Generate token and record it for user
            const token = uuid();
            await giveToken(token, user, response);

            response.send({status: 'OK', token: token});
            GLOBAL_STATS.total_login_success++;
        }
    }, {method: 'GET', mandatoryQueryParameters: ['id', 'hash']});

    // Logs out a user given their token
    await service.route('/logout', async (request, response) => {
        GLOBAL_STATS.total_logout_attempts++;
        try {
            // Remove token from cookies and the db
            const user: User = await authToken(request.query.token);
            response.cookie('token', '', {expires: new Date(0)});
            await service.db.run(`
                UPDATE users SET token = ? WHERE id = ?`, [null, user.id]);

            const msg = `Logout successfull for user ${user.id}`;
            service.log('I', msg);
            response.send({status: 'OK', data: msg});
            GLOBAL_STATS.total_logout_success++;
            
        } catch (ex) {
            service.log('E', request.query.token, ex.message);
            response.status(400).send(
                {status: 'ERR', data: ex.message});
        }
    }, {method: 'GET', mandatoryQueryParameters: ['token']});

    // Authenticates that a token is valid and belond to a certain user
    await service.route('/auth', async (request, response) => {
        GLOBAL_STATS.total_auth_attempts++;
        try {
            const user: User = await authToken(request.query.token);
            response.send({status: 'OK', id: user.id});
            GLOBAL_STATS.total_auth_success++;
        } catch (ex) {
            service.log('E', request.query.token, ex.message);
            response.status(400).send(
                {status: 'ERR', data: ex.message});
        }
    }, {method: 'GET', mandatoryQueryParameters: ['token']});

    // Maintenance loop
    const maintenance_loop = async () => {
        const rows = await service.db.all(
            `SELECT timestamp, message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`);
        const msgs = rows.map((row: any) => row.message);
        GLOBAL_STATS.last_errors = rows
            .filter((row: any, ix) => msgs.indexOf(row.message) === ix)
            .map((row: any) => `[${row.timestamp}] ${row.message}`);

        GLOBAL_STATS.loadavg = os.loadavg();
        ['login', 'logout', 'registration', 'verification'].forEach(metric => {
            GLOBAL_STATS[`total_${metric}_failures`] =
                GLOBAL_STATS[`total_${metric}_attempts`] - GLOBAL_STATS[`total_${metric}_success`];
            GLOBAL_STATS[`last_${metric}_attempts`] =
                GLOBAL_STATS[`total_${metric}_attempts`] - (GLOBAL_STATS[`last_${metric}_attempts`] || 0);
            GLOBAL_STATS[`last_${metric}_success`] =
                GLOBAL_STATS[`total_${metric}_success`] - (GLOBAL_STATS[`last_${metric}_success`] || 0);
        });

        // Delete expired tokens and pending verification
        await service.db.run(`
            DELETE FROM users WHERE verified = 0 AND timestamp < datetime('now', '-1 days')`)

        // Register with orchestrator if option was passed as argument
        if (getopt('orchestrator')) {
            const orchestrator_url = url.parse(getopt('orchestrator'));
            service.register(orchestrator_url, 'identity', 'auth');
            service.register(orchestrator_url, 'identity', 'login');
            service.register(orchestrator_url, 'identity', 'logout');
            service.register(orchestrator_url, 'identity', 'register');
            service.register(orchestrator_url, 'identity', 'verify');
        }
    };
    setInterval(maintenance_loop, 60000);
    await maintenance_loop();

    // Pick a random port
    service.start(parseInt(getopt('port')) || Math.floor(Math.random() * 8975) + 1024);
    
    // Notify other components of startup, but do not exit on failure
    await service.notify().catch(err => { /* already logged by notify() */ });
    
    return service;
}

// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
