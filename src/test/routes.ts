import { MicroService } from 'microservice'
import * as assert from 'assert';
import * as mocha from 'mocha';
import * as request from 'supertest';
import { main } from '../index';
import { auth } from '../utils';
let service: MicroService;

describe('Without email verification', () => {
    const userid1 = '1234';
    const userid2 = '5678';
    const hash = 'abcd';
    const port = Math.floor(Math.random() * 8975) + 1024;

    before(async () => {
        service = await main({test: true, email: false});
    });

    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(port);
    });
    afterEach(() => {
        service.stop();
    });

    describe('Register', () => {

        it('OK', async () => {
            const res = await request(service.server).get(`/register?id=${userid1}&hash=${hash}`).expect(200);
            const user = await service.db.get(`SELECT * FROM users WHERE id = ?`, [userid1]);
            assert.equal(!!user.verified, true);
            assert(res.header['set-cookie'].join('').match(/token=[\w\-]+;/));
        });

        it('Fails for existing ID', async () => {
            await request(service.server).get(`/register?id=${userid1}&hash=${hash}`).expect(400);
            const user = await service.db.get(`SELECT * FROM users WHERE id = ?`, [userid1]);
            assert(!!user);
        });
    });

    describe('Login', () => {

        it('OK', async () => {
            const res = await request(service.server).get(`/login?id=${userid1}&hash=${hash}`).expect(200);
            assert(res.header['set-cookie'].join('').match(/token=[\w\-]+;/));
        });

        it('Wrong password', done => {
            request(service.server).get(`/login?id=${userid1}&hash=x${hash}`).expect(400, done);
        });

        it('Wrong user', done => {
            request(service.server).get(`/login?id=${userid2}&hash=${hash}`).expect(400, done);
        });
    });

    describe('Auth', () => {

        it('OK', async () => {
            const token = (await service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            const res = await request(service.server).get(`/auth?token=${token}`).expect(200);
        });

        it('Using helper function', async () => {
            const token = (await service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            assert(auth(`http://127.0.0.1:${port}`, token, userid1));
        });
    });

    describe('Logout', () => {
        
        it('OK', async () => {
            const token = (await service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            const res = await request(service.server).get(`/logout?token=${token}`).expect(200);
            assert(res.header['set-cookie'].join('').match(/token=;/));
            await request(service.server).get(`/auth?token=${token}`).expect(400);
        });
    });
});

describe('With email verification', () => {
    const email = 'fake@email.com';
    const userid1 = '1234';
    const userid2 = '5678';
    const hash = 'abcd';

    before(async () => {
        service = await main({test: true, email: true});
    });

    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });

    describe('Register', () => {
        
        it('Fails without email', async () => {
            await request(service.server).get(`/register?id=${userid1}&hash=${hash}`).expect(400);
        });
    });

    describe('Verify', () => {
        
        it('Provided token', async () => {
            // Register user
            const res = await request(service.server).get(`/register?id=${userid1}&hash=${hash}&email=${email}`).expect(200);
            const user = await service.db.get(`SELECT * FROM users WHERE id = ?`, [userid1]);
            assert.equal(!!user.verified, false);
            assert(!res.header['set-cookie'] || !res.header['set-cookie'].join('').match(/token/));

            // Verify token
            const token = (await service.db.get(`SELECT token FROM users WHERE id = ?`, userid1)).token;
            await request(service.server).get(`/verify?token=${token}&email=${email}`).expect(200);
        });
    });
});