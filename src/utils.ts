import { Requests } from 'ootils';
import * as url from 'url';

/**
 * Returns true/false if the provided token belongs to the specified user ID
 * @param endpoint URL of the authentication endpoint
 * @param token session token to authenticate
 * @param user_id expected user ID
 */
export async function auth(endpoint: string | url.Url, token: string, user_id: string, orchestrator = false) {
    try {
        let auth_url: url.Url = null;
        if (typeof endpoint === 'string') {
            auth_url = url.parse(endpoint);
        } else {
            auth_url = endpoint;
        }
        if (orchestrator) {
            auth_url.pathname = `/service/identity/auth`;
        }
        auth_url.query = {token: token};
    const res = JSON.parse(await Requests.get(url.format(auth_url)));
        return res.status === 'OK' && res.id === user_id;
    } catch(ex) {
        return false;
    }
}